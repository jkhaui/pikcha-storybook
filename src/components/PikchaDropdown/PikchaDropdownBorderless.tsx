// @ts-nocheck
import React, {useState} from "react";
import Select from "react-select";
import {useTheme} from "@material-ui/core/styles";
import {ArrowDropDown} from "@material-ui/icons";
import PikchaLoader from "../loaders/PikchaLoader";
import PikchaDropdownMenuList from "./PikchaDropdownMenuList";
import "./PikchaDropdown.css";

const PikchaDropdownBorderless = ({
                                    label,
                                    className = "",
                                    style = {},
                                    fullWidth,
                                    compactListSize,
                                    noTopMargin,
                                    noHelperClass,
                                    required,
                                    classNameOuter,
                                    disabled,
                                    placeholder = "",
                                    isMulti = false,
                                    closeMenuOnSelect = true,
                                    valueColourDisabled,
                                    FieldLabelProps = {},
                                    ...rest
                                  }) => {
  const [isFocused, setIsFocused] = useState(false);

  const {palette, shape, typography} = useTheme();
  return (
    <div className={`${fullWidth ? "full-width" : ""} ${className}`}
         style={style}
         onFocusCapture={() => setIsFocused(true)}
         onBlurCapture={() => setIsFocused(false)}>
      <Select
        isMulti={isMulti}
        closeMenuOnSelect={closeMenuOnSelect}
        className="Dropdown-innerContainer"
        isDisabled={disabled ? disabled : false}
        isSearchable={false}
        components={{
          IndicatorSeparator: null,
          DropdownIndicator: () => <ArrowDropDown />,
          MenuList: (props) => <PikchaDropdownMenuList {...props} />
        }}
        aria-label="Select"
        placeholder={placeholder}
        loadingMessage={() => <PikchaLoader linearVariant />}
        noOptionsMessage={() => ""}
        styles={{
          container: (provided) => ({
            ...provided,
            width: '5rem',
            padding: '0',
            fontSize: '0.9rem',
            fontWeight: typography.fontWeightMedium,
            borderRadius: shape.borderRadius,
            borderColor: 'none',
          }),
          control: (provided) => ({
            ...provided,
            width: 'auto',
            minHeight: '30px',
            height: '30px',
            padding: 0,
            borderWidth: 0,
            borderColor: 'none',
            boxShadow: "none",
            cursor: "pointer",
            borderRadius: shape.borderRadius,
          }),
          indicatorsContainer: (provided) => ({
            ...provided,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'space-around',
            borderRadius: shape.borderRadius,
            cursor: "pointer",
            height: '30px',
          }),
          valueContainer: (provided) => ({
            ...provided,
            padding: 0,
            margin: 0,
            borderRadius: shape.borderRadius,
            color: palette.text.primary,
            height: '30px'
          }),
          input: (provided) => ({
            ...provided,
            paddingTop: 0,
            paddingBottom: 0,
            margin: 0,
            padding: 0,
          }),
          menuList: () => ({
            cursor: "default",
            borderRadius: shape.borderRadius,
            maxHeight: '10rem',
            paddingBottom: '4',
            position: "relative",
            boxSizing: "border-box"
          }),
          menu: (provided) => ({
            ...provided,
            marginTop: 0,
            border: `1px solid ${palette.secondary.main}`,
            borderRadius: shape.borderRadius,
            position: "absolute",
            borderTop: 0,
            paddingBottom: 8,
            zIndex: 999
          }),
          placeholder: (provided) => ({
            ...provided,
            paddingLeft: 14,
            color: valueColourDisabled && palette.action.disabled,
            fontSize: typography.smallest,
          }),
          singleValue: (provided, {isDisabled}) => ({
            ...provided,
            userSelect: isDisabled ? "none" : undefined,
            color: !isDisabled && !valueColourDisabled
              ? palette.text.primary
              : palette.action.disabled,
            paddingLeft: 0,
          }),
          option: (provided, {isFocused, isSelected}) => ({
            ...provided,
            cursor: "pointer",
            marginLeft: 1,
            fontSize: 12,
            textTransform: "capitalize",
            color: palette.grey.mediumDark,
            paddingTop: 4,
            paddingBottom: 4,
            backgroundColor: isFocused ? palette.grey.light : null,
            ":active": {
              ...provided[":active"],
              color: isSelected ? palette.secondary.contrastText : null,
              backgroundColor: isSelected ? palette.secondary.main : null,
            },
          }),
        }}
        {...rest}
      />
    </div>
  );
};

export default PikchaDropdownBorderless;
