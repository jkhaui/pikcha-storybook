// @ts-nocheck
import React from "react";
import KeyboardArrowDownRoundedIcon from "@material-ui/icons/KeyboardArrowDownRounded";
import { components } from "react-select";

export default ({ innerRef, innerProps }) => {
  const { DropdownIndicator } = components;

  return (
    <DropdownIndicator
      className="pointer"
      aria-label="Dropdown Indicator"
      //ref={innerRef}
      {...innerProps}
    >
      <KeyboardArrowDownRoundedIcon />
    </DropdownIndicator>
  );
};
