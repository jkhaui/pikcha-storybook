// @ts-nocheck
import React from "react";
import { useTheme } from "@material-ui/core";

import "./PikchaLoader.css";

export default ({ size, color, message, linearVariant, marginTop=null, marginBottom=null, ...rest }) => {
  const { palette } = useTheme();

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        justifyContent: "center",
      }}
    >
      <div id="spinner" style={{marginTop: marginTop, marginBottom: marginBottom}}/>
    </div>
  );
};
