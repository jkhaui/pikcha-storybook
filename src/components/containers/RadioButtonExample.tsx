// @ts-nocheck
import React, { useCallback, useEffect, useState } from "react";
import clsx from "clsx";
import {
  FormControl,
  FormControlLabel,
  makeStyles,
  Radio,
  RadioGroup,
} from "@material-ui/core";

export const ASPECT_RATIOS = [
  {
    id: 2,
    label: "3:2",
    cropperVal: 3 / 2,
    orientation: "landscape",
    value: "3,2",
  },
  {
    label: "4:3",
    id: 6,
    cropperVal: 4 / 3,
    orientation: "landscape",
    value: "4,3",
  },
  {
    id: 1,
    label: "5:4",
    cropperVal: 5 / 4,
    orientation: "landscape",
    value: "5,4",
  },
  {
    label: "2:3",
    id: 5,
    cropperVal: 2 / 3,
    orientation: "portrait",
    value: "2,3",
  },
  {
    id: 3,
    label: "3:4",
    cropperVal: 3 / 4,
    orientation: "portrait",
    value: "3,4",
  },
  {
    id: 4,
    label: "4:5",
    cropperVal: 4 / 5,
    orientation: "portrait",
    value: "4,5",
  },
  {
    label: "1:1",
    id: 7,
    cropperVal: 1,
    orientation: "square",
    value: "1,1",
  },
  {
    id: 8,
    label: "1:2",
    cropperVal: 1 / 2,
    orientation: "portrait",
    value: "1,2",
  },
  {
    id: 9,
    label: "1:3",
    cropperVal: 1 / 3,
    orientation: "portrait",
    value: "1,3",
  },
  {
    id: 10,
    label: "2:1",
    cropperVal: 2 / 1,
    orientation: "landscape",
    value: "2,1",
  },
  {
    id: 11,
    label: "3:1",
    cropperVal: 3 / 1,
    orientation: "landscape",
    value: "3,1",
  },
];
const mapAspectRatioToRadioButtonKey = (ratio) => {
  switch (ratio) {
    case "2,1":
      return 9;
    case "3,1":
      return 10;
    case "1,2":
      return 7;
    case "1,3":
      return 8;
    case "2,3":
      return 3;
    case "3,4":
      return 4;
    case "4,5":
      return 5;
    case "3,2":
      return 0;
    case "4,3":
      return 1;
    case "5,4":
      return 2;
    case "1,1":
      return 6;
    default:
      return 0;
  }
};
export const useStyles = makeStyles(({ palette, typography, spacing }) => ({
  root: {
    padding: 4,
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  icon: {
    borderRadius: 50,
    width: "1rem",
    height: "1rem",
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#F5F8FA",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto rgba(19,124,189,.6)",
      outlineOffset: 2,
    },
    "input:hover ~ &": {
      backgroundColor: "#EBF1F5",
    },
  },
  checkedIcon: {
    backgroundColor: "black",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: "1rem",
      height: "1rem",
      backgroundImage: `radial-gradient(${palette.common.white}, ${palette.common.white} 28%,transparent 32%)`,
      content: "\"\"",
    },
    "input:hover ~ &": {
      backgroundColor: "black",
    },
  },
  formControl: {
    padding: spacing(5),
    width: "100%",
  },
  label: {
    fontWeight: typography.fontWeightMedium,
    fontFamily: typography.fontFamilyMontserrat,
    fontSize: typography.standard,
    marginBottom: "0.5em",
  },
}));

export default () => {
  const classes = useStyles();
  const [aspectRatio, setAspectRatio] = useState("4,5");
  const [step1SelectedRatio, setStep1SelectedRatio] = useState(1);
  const [selectedRatio, setSelectedRatio] = useState(ASPECT_RATIOS[0]);
  useEffect(() => {
    ASPECT_RATIOS.forEach((ratio) => {
      if (ratio.id === step1SelectedRatio) {
        setSelectedRatio(ratio);
      }
    });
  }, [step1SelectedRatio]);
  const updateAspectRatio = useCallback(
    (aspectRatio) => {
      setAspectRatio(aspectRatio);
    },
    [setAspectRatio],
  );
  const handleChangeSelectedRatio = (value) => {
    setSelectedRatio(value);
  };
  useEffect(() => {
    const detectedAspectRatio =
      ASPECT_RATIOS[mapAspectRatioToRadioButtonKey(aspectRatio)];

    // @ts-ignore
    setAspectRatio(detectedAspectRatio.id);
    setSelectedRatio(detectedAspectRatio);
  }, [step1SelectedRatio, aspectRatio, aspectRatio]);

  const getAspectRatios = (orientation) => {
    let ratios = [];
    if (orientation === "landscape") {
      ASPECT_RATIOS.forEach((item, index) => {
        if (item.orientation === "landscape") {
          ratios.push(
            <FormControlLabel
              key={index}
              value={item.id}
              control={
                <Radio
                  className={classes.root}
                  disableRipple
                  color="default"
                  checkedIcon={
                    <span className={clsx(classes.icon, classes.checkedIcon)} />
                  }
                  icon={<span className={classes.icon} />}
                />
              }
              label={item.label}
            />,
          );
        }
      });
    } else if (orientation === "portrait") {
      ASPECT_RATIOS.forEach((item, index) => {
        if (item.orientation === "portrait") {
          ratios.push(
            <FormControlLabel
              key={index}
              value={item.id}
              control={
                <Radio
                  className={classes.root}
                  disableRipple
                  color="default"
                  checkedIcon={
                    <span className={clsx(classes.icon, classes.checkedIcon)} />
                  }
                  icon={<span className={classes.icon} />}
                />
              }
              label={item.label}
            />,
          );
        }
      });
    } else {
      ASPECT_RATIOS.forEach((item, index) => {
        if (item.orientation === "square") {
          ratios.push(
            <FormControlLabel
              key={index}
              value={item.id}
              control={
                <Radio
                  className={classes.root}
                  disableRipple
                  color="default"
                  checkedIcon={
                    <span className={clsx(classes.icon, classes.checkedIcon)} />
                  }
                  icon={<span className={classes.icon} />}
                />
              }
              label={item.label}
            />,
          );
        }
      });
    }

    return ratios;
  };
  const addAspectRatio = ({ target: { value } }) => {
    const id = parseInt(value);
    let selRatio = null;
    setAspectRatio(id);
    handleChangeSelectedRatio(id);
    ASPECT_RATIOS.forEach((ratio) => {
      if (ratio.id === id) {
        selRatio = ratio;

        setSelectedRatio(ratio);
      }
    });
    if (selRatio && selRatio.value) {
      updateAspectRatio(selRatio.value);
    }
  };
  return (
    <div
      style={{
        display: "block",
      }}
    >
      <FormControl
        size="small"
        component="div"
        style={{
          minWidth: 144,
          display: "block",
        }}
      >
        <RadioGroup
          aria-label="Aspect Ratio"
          name="Aspect Ratio"
          value={aspectRatio}
          style={{
            display: "block",
          }}
          onChange={addAspectRatio}
        >
          {getAspectRatios("portrait")}
        </RadioGroup>
      </FormControl>
      <FormControl
        size="small"
        component="div"
        style={{
          minWidth: 144,
          display: "block",
        }}
      >
        <RadioGroup
          aria-label="Aspect Ratio"
          name="Aspect Ratio"
          value={aspectRatio}
          style={{
            display: "block",
          }}
          onChange={addAspectRatio}
        >
          {getAspectRatios("landscape")}
        </RadioGroup>
      </FormControl>
      <FormControl
        size="small"
        style={{
          minWidth: 144,
          display: "block",
        }}
        component="div"
      >
        <RadioGroup
          aria-label="Aspect Ratio"
          name="Aspect Ratio"
          style={{
            display: "block",
          }}
          value={aspectRatio}
          onChange={addAspectRatio}
        >
          {getAspectRatios("square")}
        </RadioGroup>
      </FormControl>
    </div>
  );
}
