import React from "react";

import isMobile from "./isMobile";

export default ({ children }) => {
  return isMobile() ? children : null;
};
