import { useMediaQuery } from "react-responsive";

export default () => {
  const isMobileLandscape = useMediaQuery({ minWidth: 568, maxWidth: 991 });
  const isOrientationLandscape = useMediaQuery({ orientation: "landscape" });

  return isMobileLandscape && isOrientationLandscape;
};
