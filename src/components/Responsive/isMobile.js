import { useMediaQuery } from "react-responsive";

export default () => {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  return isMobile;
};
