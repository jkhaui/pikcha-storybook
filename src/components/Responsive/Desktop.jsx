import React from "react";

import isDesktop from "./isDesktop";

export default ({ children }) => {
  return isDesktop() ? children : null;
};
