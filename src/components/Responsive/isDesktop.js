import { useMediaQuery } from "react-responsive";

export default () => {
  const isDesktop = useMediaQuery({ minWidth: 993 });
  return isDesktop;
};
