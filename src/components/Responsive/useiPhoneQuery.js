﻿import {mobileVendor, deviceType} from "react-device-detect";

export const useiPhoneQuery = () => {
  // console.log('window.width=', window.screen.availWidth);
  // console.log('window.height=', window.screen.availHeight);

  return (mobileVendor.toLowerCase().includes("iphone") || mobileVendor.toLowerCase().includes("apple")) &&
    deviceType.toLowerCase().includes('mobile');
}
