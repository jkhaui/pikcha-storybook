import { useMediaQuery } from "react-responsive";

export default () => {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const isOrientationPortrait = useMediaQuery({ orientation: "portrait" });
  return isMobile && isOrientationPortrait;
};
