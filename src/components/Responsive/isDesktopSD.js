import { useMediaQuery } from "react-responsive";

export default () => {
  const isDesktopSD = useMediaQuery({ minWidth: 993, maxWidth: 1200 });
  return isDesktopSD;
};
