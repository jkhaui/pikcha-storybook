import { useMediaQuery } from "react-responsive";

export default () => {
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 993 });
  return isTablet;
};
