import { useMediaQuery } from "react-responsive";

export default () => {
  const isDesktopHD = useMediaQuery({ minWidth: 1201 });
  return isDesktopHD;
};
