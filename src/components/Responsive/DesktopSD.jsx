import React from "react";

import isDesktopSD from "./isDesktopSD";

export default ({ children }) => {
  return isDesktopSD() ? children : null;
};
