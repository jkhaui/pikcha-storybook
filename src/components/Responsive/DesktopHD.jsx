import React from "react";

import isDesktopHD from "./isDesktopHD";

export default ({ children }) => {
  return isDesktopHD() ? children : null;
};
