import React from "react";

import isTablet from "./isTablet";

export default ({ children }) => {
  return isTablet() ? children : null;
};
