﻿// @ts-nocheck
import {NavHashLink as HashLink} from "react-router-hash-link";

export const NavHashLink = ({children, ...rest}) => {
  return (
    <HashLink smooth activeClassName="hashlink" {...rest}>
      {children}
    </HashLink>
  );
}
