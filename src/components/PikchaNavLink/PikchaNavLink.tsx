// @ts-nocheck
import React from "react";
import { useTheme } from "@material-ui/core/styles";

import {PikchaLink} from "../PikchaLink";

import "./PikchaNavLink.css";

export default ({ children, to }) => {
  const { palette } = useTheme();

  return (
    <PikchaLink
      to={to}
      style={{
        color: palette.grey.darker,
        fontWeight: 600,
      }}
    >
      {children}
    </PikchaLink>
  );
};
